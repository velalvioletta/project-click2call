package id.co.telkom.click2call.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;

import id.co.telkom.click2call.R;
import id.co.telkom.click2call.activities.SubActivity;

public class CallCenterFragment extends Fragment {

    public CallCenterFragment() {
    }

    LinearLayout view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = (LinearLayout) inflater.inflate(R.layout.fragment_callcenter, container, false);

        RelativeLayout relativeLayoutWifiid = view.findViewById(R.id.rl_wifiid);
        relativeLayoutWifiid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SubActivity.class);
                startActivity(intent);
            }
        });

        getActivity();
        return view;
    }

}