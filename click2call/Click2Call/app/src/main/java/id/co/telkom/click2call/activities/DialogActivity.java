package id.co.telkom.click2call.activities;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import id.co.telkom.click2call.App;
import id.co.telkom.click2call.R;

public class DialogActivity extends AppCompatActivity implements View.OnClickListener {


    TextView phoneNumber;
    ImageView btnEndCall, btnSpeaker, ivShow;

    public String sipAdress = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_dialog);

        phoneNumber = (TextView) findViewById(R.id.phone_number);
        btnEndCall = (ImageView) findViewById(R.id.btn_end);
        btnSpeaker = (ImageView) findViewById(R.id.btn_speaker);

//        if (getIntent().getExtras() != null) {
            sipAdress = "082372210507";
            phoneNumber.setText(sipAdress);
            initiateCall();
//        }

        btnEndCall.setOnClickListener(this);
        btnSpeaker.setOnClickListener(this);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private void initiateCall() {

        if (App.getSipStack() == null){
            Toast.makeText(this, "ERROR SIP STACK", Toast.LENGTH_LONG).show();
        } else {
            App.getSipStack().Call(-1, sipAdress);
        }
    }
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_end) {
            if (App.getSipStack() != null) {
                App.getSipStack().Hangup();
                finish();
            }
        }else if(v.getId() == R.id.btn_speaker){
            if(App.getSipStack() != null){
                if(App.getSipStack().IsLoudspeaker()){
                    App.getSipStack().SetSpeakerMode(false);
                }else{
                    App.getSipStack().SetSpeakerMode(true);
                }
            }

        }
    }
}
