package id.co.telkom.click2call.fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import id.co.telkom.click2call.R;
import id.co.telkom.click2call.activities.NavigationDrawer;
import id.co.telkom.click2call.utils.Constants;

public class LoginFragment extends Fragment implements OnClickListener {
	private static View view;

	private static EditText username, password;
	private static Button loginButton;
	private static TextView forgotPassword, signUp;
	private static CheckBox show_hide_password;
	private static LinearLayout loginLayout;
	private static Animation shakeAnimation;
	private static FragmentManager fragmentManager;

	public LoginFragment() {
	}

	public static void onBackPressed() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fragment_login, container, false);
		initViews();
		setListeners();
		return view;
	}

	private void initViews() {
		fragmentManager = getActivity().getSupportFragmentManager();
		username = (EditText) view.findViewById(R.id.login_username);
		password = (EditText) view.findViewById(R.id.login_password);
		loginButton = (Button) view.findViewById(R.id.loginBtn);
		forgotPassword = (TextView) view.findViewById(R.id.forgot_password);
		signUp = (TextView) view.findViewById(R.id.createAccount);
		show_hide_password = (CheckBox) view.findViewById(R.id.show_hide_password);
		loginLayout = (LinearLayout) view.findViewById(R.id.login_layout);

		shakeAnimation = AnimationUtils.loadAnimation(getActivity(),
				R.anim.shake);

		// Setting text selector over textviews

		@SuppressLint("ResourceType") XmlResourceParser xrp = getResources().getXml(R.drawable.text_selector);

		try {
			ColorStateList csl = ColorStateList.createFromXml(getResources(),
					xrp);
			forgotPassword.setTextColor(csl);
			show_hide_password.setTextColor(csl);
			signUp.setTextColor(csl);
		} catch (Exception e) {
		}
	}

	// Set Listeners
	private void setListeners() {
		loginButton.setOnClickListener(this);
		forgotPassword.setOnClickListener(this);
		signUp.setOnClickListener(this);

		// Set check listener over checkbox for showing and hiding password
		show_hide_password
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton button,
												 boolean isChecked) {
						if (isChecked) {
							show_hide_password.setText(R.string.hide_pwd);
							password.setInputType(InputType.TYPE_CLASS_TEXT);
							password.setTransformationMethod(HideReturnsTransformationMethod
									.getInstance());
						} else {
							show_hide_password.setText(R.string.show_pwd);
							password.setInputType(InputType.TYPE_CLASS_TEXT
									| InputType.TYPE_TEXT_VARIATION_PASSWORD);
							password.setTransformationMethod(PasswordTransformationMethod
									.getInstance());// hide password
						}
					}
				});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.loginBtn:
				checkValidation();
				break;

			case R.id.forgot_password:
				// Replace forgot password fragment with animation
				fragmentManager
						.beginTransaction()
						.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
						.replace(R.id.frameContainer,
								new ForgotPasswordFragment(),
								Constants.ForgotPassword_Fragment).commit();
				break;

			case R.id.createAccount:
				// Replace signup frgament with animation
				fragmentManager
						.beginTransaction()
						.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
						.replace(R.id.frameContainer, new RegisterFragment(),
								Constants.SignUp_Fragment).commit();
				break;
		}

	}

	// Check Validation before login
	private void checkValidation() {

//		final String getEmailId = username.getText().toString();
//		final String getPassword = password.getText().toString();
//
//		if (getEmailId.equals("") || getEmailId.length() == 0
//				|| getPassword.equals("") || getPassword.length() == 0) {
//
//			new CustomToast().Show_Toast(getActivity(), view,
//					"Enter both credentials.");
//		} else {
			final ProgressDialog dialog = new ProgressDialog(getActivity());
			dialog.setMessage("Loading...");
			dialog.setProgressStyle(dialog.STYLE_SPINNER);
			dialog.setIndeterminate(true);
			dialog.setCancelable(false);
			dialog.show();

//			final UserDAO dao = App.getDb().userDAO();
//			if (dao.login(getEmailId, getPassword) != null) {
				Runnable progressRunnable = new Runnable() {

					@Override
					public void run() {
						dialog.cancel();

						Intent intent = new Intent(getActivity(), NavigationDrawer.class);
						startActivity(intent);
					}
				};
				Handler pdCanceller = new Handler();
				pdCanceller.postDelayed(progressRunnable, 3000);

//			} else {
//				dialog.dismiss();
//				new CustomToast().Show_Toast(getActivity(), view,
//						"Akun belum terdaftar");
//
//			}
//		}
	}
}