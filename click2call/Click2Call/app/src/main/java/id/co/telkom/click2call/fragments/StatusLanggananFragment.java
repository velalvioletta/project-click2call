package id.co.telkom.click2call.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;

import id.co.telkom.click2call.R;

public class StatusLanggananFragment extends Fragment {

    public StatusLanggananFragment(){}
    RelativeLayout view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        view = (RelativeLayout) inflater.inflate(R.layout.fragment_statuslangganan, container, false);

        getActivity().setTitle("Status Langganan");

        return view;
    }
}
