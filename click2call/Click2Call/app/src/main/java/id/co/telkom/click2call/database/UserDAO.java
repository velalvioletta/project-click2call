package id.co.telkom.click2call.database;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import id.co.telkom.click2call.entities.UserDB;

@Dao
public interface UserDAO {

    @Insert
    long insertUser(UserDB userDB);

    @Update
    int updateUser(UserDB userDB);

    @Delete
    int deleteUser(UserDB userDB);

    @Query("SELECT * FROM user")
    List<UserDB> fetchAll();

    @Query("SELECT * FROM user WHERE email = :email AND password = :password")
    UserDB login(String email, String password);

}