package id.co.telkom.click2call.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import id.co.telkom.click2call.entities.UserDB;


@Database(entities = {UserDB.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract UserDAO userDAO();
}