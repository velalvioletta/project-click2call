package id.co.telkom.click2call.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.navigation.NavigationView;

import id.co.telkom.click2call.R;
import id.co.telkom.click2call.fragments.CallCenterFragment;
import id.co.telkom.click2call.fragments.HomeFragment;
import id.co.telkom.click2call.fragments.LoginFragment;
import id.co.telkom.click2call.fragments.ProfilFragment;
import id.co.telkom.click2call.fragments.RiwayatFragment;
import id.co.telkom.click2call.fragments.SettingsFragment;
import id.co.telkom.click2call.fragments.StatusLanggananFragment;
import id.co.telkom.click2call.fragments.TagihanFragment;
import id.co.telkom.click2call.views.Root;

public class NavigationDrawer extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public Object setOnClickListener;
    Toolbar toolbar;
    DrawerLayout drawer;
    NavigationView navigationView;
    FragmentManager fragmentManager;
    Fragment fragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (savedInstanceState == null) {
            fragment = new Root();
            callFragment(fragment);
        }
    }


    @Override
    public void onBackPressed() {
        drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
        } else {
            super.onBackPressed();
        }
    }

    @Override
            public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the HomeFragment/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(getApplicationContext(), "Action SettingsFragment", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item){
            int id = item.getItemId();

            if (id == R.id.nav_home) {
                fragment = new HomeFragment();
                callFragment(fragment);
            } else if (id == R.id.nav_profil) {
                fragment = new ProfilFragment();
                callFragment(fragment);
            } else if (id == R.id.nav_statuslangganan) {
                fragment = new StatusLanggananFragment();
                callFragment(fragment);
            } else if (id == R.id.nav_tagihan) {
                fragment = new TagihanFragment();
                callFragment(fragment);
            } else if (id == R.id.nav_riwayat) {
                fragment = new RiwayatFragment();
                callFragment(fragment);
            } else if (id == R.id.nav_callcenter) {
                fragment = new CallCenterFragment();
                callFragment(fragment);
            } else if (id == R.id.nav_settings) {
                fragment = new SettingsFragment();
                callFragment(fragment);
            } else if (id == R.id.nav_logout) {
                final AlertDialog.Builder dialog = new AlertDialog.Builder(NavigationDrawer.this);
                dialog.setTitle("Confirm Exit");
                dialog.setMessage("Are you sure you want to exit?");
                dialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        fragment = new LoginFragment();
                        callFragment(fragment);
                    }
                });
                dialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
            drawer = findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
            return true;

    }

    private void callFragment(Fragment fragment) {
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.frame_container,fragment)
                .commit();
        }
}