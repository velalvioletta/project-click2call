package id.co.telkom.click2call.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;

import id.co.telkom.click2call.R;

public class HomeFragment extends Fragment {

    public HomeFragment(){}
    RelativeLayout view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        view = (RelativeLayout) inflater.inflate(R.layout.fragment_home, container, false);

        getActivity().setTitle("HomeFragment");

        return view;
    }
}
