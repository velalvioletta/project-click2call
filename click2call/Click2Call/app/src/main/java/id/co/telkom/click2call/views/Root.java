package id.co.telkom.click2call.views;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;

import id.co.telkom.click2call.R;

public class Root extends Fragment {

    public Root(){}
        View rootView;

        @Override
        public View onCreateView (LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState){
            rootView = inflater.inflate(R.layout.fragment_home, container, false);

            getActivity().setTitle("Velariza");

            return rootView;
        }

    }