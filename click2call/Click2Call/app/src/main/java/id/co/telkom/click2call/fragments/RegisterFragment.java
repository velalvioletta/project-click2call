package id.co.telkom.click2call.fragments;

import android.app.DatePickerDialog;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import id.co.telkom.click2call.App;
import id.co.telkom.click2call.R;
import id.co.telkom.click2call.activities.MainActivity;
import id.co.telkom.click2call.database.UserDAO;
import id.co.telkom.click2call.entities.UserDB;
import id.co.telkom.click2call.utils.Constants;
import id.co.telkom.click2call.views.CustomToast;


public class RegisterFragment extends Fragment implements OnClickListener {
	private static View view;
	private static EditText fullName;
	private static EditText emailId;
	private static EditText password;
	private static EditText birthday;
	private static TextView login;
	private static Button signUpButton;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fragment_register, container, false);
		initViews();
		setListeners();
		return view;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		birthday.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showDateDialog();
			}
		});

		birthday.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					showDateDialog();
				}
			}
		});
	}

	private void showDateDialog() {
		final Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);
		new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int month, int day) {
				Log.d("CLICK", "year " + year + " month " + month);
				String date = day + "-" + (month + 1) + "-" + year;
				birthday.setText(date);
			}
		}, year, month, day).show();
	}

	// inisialisasi
	private void initViews() {
		fullName = (EditText) view.findViewById(R.id.fullName);
		emailId = (EditText) view.findViewById(R.id.userEmailId);
		password = (EditText) view.findViewById(R.id.password);
		birthday = (EditText) view.findViewById(R.id.birthday);
		signUpButton = (Button) view.findViewById(R.id.signUpBtn);
		login = (TextView) view.findViewById(R.id.already_user);
	}

	private void setListeners() {
		signUpButton.setOnClickListener(this);
		login.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.signUpBtn:
				checkValidation();
				break;

			case R.id.already_user:
				new MainActivity().replaceLoginFragment(new LoginFragment());
				break;
		}

	}

	private void checkValidation() {

		String getFullName = fullName.getText().toString();
		String getEmailId = emailId.getText().toString();
		String getPassword = password.getText().toString();
		String getBirthday = birthday.getText().toString();

		// Pattern match for email id
		Pattern p = Pattern.compile(Constants.regEx);
		Matcher m = p.matcher(getEmailId);

		if (getFullName.equals("") || getFullName.length() == 0
				|| getEmailId.equals("") || getEmailId.length() == 0
				|| getPassword.equals("") || getPassword.length() == 0
				|| getBirthday.equals("")
				|| getBirthday.length() == 0) {

			new CustomToast().Show_Toast(getActivity(), view,
					"All fields are required.");

		} else if (!m.find()) {
			new CustomToast().Show_Toast(getActivity(), view,
					"Your Email Id is Invalid.");
		} else {
			UserDAO dao = App.getDb().userDAO();
			UserDB user = new UserDB();
			user.setBirthday(getBirthday);
			user.setEmail(getEmailId);
			user.setName(getFullName);
			user.setPassword(getPassword);

			long result = 0;
			try {
				result = dao.insertUser(user);

				if (result == -1) {
					new CustomToast().Show_Toast(getActivity(), view,
							"Register gagal!");
				} else {
					new CustomToast().Show_Toast(getActivity(), view,
							"Register sukses! Silakan Login");
				}
			}catch (SQLiteConstraintException e){
				if(e.getMessage().contains("user.email")){
					new CustomToast().Show_Toast(getActivity(), view,
							"Email telah terdaftar");
				}else {
					new CustomToast().Show_Toast(getActivity(), view,
							e.getMessage());
				}
			}
		}
	}
}
