package id.co.telkom.click2call.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import java.util.List;

import id.co.telkom.click2call.App;
import id.co.telkom.click2call.R;
import id.co.telkom.click2call.entities.UserDB;
import id.co.telkom.click2call.fragments.LoginFragment;
import id.co.telkom.click2call.utils.Constants;

public class MainActivity extends AppCompatActivity {

    private static FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentManager = getSupportFragmentManager();

        if (savedInstanceState == null) {
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.frameContainer, new LoginFragment(),
                            Constants.Login_Fragment).commit();
        }

        // On close icon click finish activity
        findViewById(R.id.close_activity).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        finish();
                    }

                });

        List<UserDB> userDBList = App.getDb().userDAO().fetchAll();
        for (int i = 0; i < userDBList.size(); i++) {
            Log.e("User", userDBList.get(i).getName() + " - " + userDBList.get(i).getEmail() + " - " +
                    userDBList.get(i).getPassword() + " - " + userDBList.get(i).getBirthday());
        }
    }


    public void replaceLoginFragment(LoginFragment loginFragment) {
        fragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.left_enter, R.anim.right_out)
                .replace(R.id.frameContainer, new LoginFragment(),
                        Constants.Login_Fragment).commit();
    }

    @Override
    public void onBackPressed() {

        Fragment SignUp_Fragment = fragmentManager
                .findFragmentByTag(Constants.SignUp_Fragment);
        Fragment ForgotPassword_Fragment = fragmentManager
                .findFragmentByTag(Constants.ForgotPassword_Fragment);

        if (SignUp_Fragment != null)
            replaceLoginFragment(new LoginFragment());
        else if (ForgotPassword_Fragment != null)
            replaceLoginFragment(new LoginFragment());
        else
            super.onBackPressed();
    }

}