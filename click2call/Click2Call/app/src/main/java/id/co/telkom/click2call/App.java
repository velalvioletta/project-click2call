package id.co.telkom.click2call;

import android.app.Application;
import android.net.sip.SipAudioCall;
import android.net.sip.SipManager;
import android.net.sip.SipProfile;

import androidx.room.Room;

import com.mizuvoip.jvoip.SipStack;

import id.co.telkom.click2call.database.AppDatabase;

public class App extends Application {

    private static SipManager sipManager;
    private static SipProfile sipProfile;
    private static SipAudioCall sipAudioCall;
    private static SipStack mysipclient;
    public static App instance;
    public static AppDatabase db;

    @Override
    public void onCreate() {
        super.onCreate();

        mysipclient = new SipStack();
        mysipclient.Init(this);

        instance= this;
        db = Room.databaseBuilder(this, AppDatabase.class, "user")
                .allowMainThreadQueries()
                .build();
    }

    public static App getInstance() {
        return instance;
    }

    public static AppDatabase getDb() {
        return db;
    }

    public static SipManager getSipManager() {
        return sipManager;
    }

    public static SipProfile getSipProfile() {
        return sipProfile;
    }

    public static SipAudioCall getSipAudioCall() {
        return sipAudioCall;
    }

    public static void setSipAudioCall(SipAudioCall audioCall) {
        sipAudioCall = audioCall;
    }

    public static SipStack getSipStack(){ return mysipclient;}

}
