package id.co.telkom.click2call.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.sip.SipException;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import id.co.telkom.click2call.App;
import id.co.telkom.click2call.R;

public class SubActivity extends AppCompatActivity  {

    private static final int CALL_ADDRESS = 1;
    private static final int VIDEO_ADDRESS = 2;
    private static final int HANG_UP = 4;
    private static final int PERMISSION_ALL = 99;

    String[] PERMISSIONS = {
            Manifest.permission.USE_SIP,
            Manifest.permission.RECORD_AUDIO
    };

    private String params = "serveraddress=10.14.16.99:5070\r\nusername=mobile\r\npassword=1234\r\nloglevel=5";
    public String sipAdress = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);

        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        } else {
            initializeLocalProfile();
        }

        RelativeLayout relativeLayoutBahasa = findViewById(R.id.rl_bahasa);
        relativeLayoutBahasa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(SubActivity.this, DialogActivity.class);
                startActivity(intent1);
            }
        });

    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ALL:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    initializeLocalProfile();
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        } else {
            initializeLocalProfile();
        }
    }

    private void initializeLocalProfile() {
        if (App.getSipStack() != null){

        }

        App.getSipStack().SetParameters(params.trim());
        App.getSipStack().Start();
    }

    public void updateStatus(final String status){
    this.runOnUiThread(new Runnable() {
        @Override
        public void run() {
        }
    });
}
    public boolean onCreateOptionsMenu(Menu menu){
    return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case CALL_ADDRESS:
                showDialog(CALL_ADDRESS);
                break;
            case VIDEO_ADDRESS:
                showDialog(VIDEO_ADDRESS);
                break;
            case HANG_UP:
                if (App.getSipAudioCall() != null) {
                    try {
                        App.getSipAudioCall().endCall();
                    } catch (SipException se) {
                        Log.d("onOptionsItemSelected",
                                "Error ending call.", se);
                    }
                    App.getSipAudioCall().close();
                }
                break;
        }
        return true;
    }
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        }
        return null;
    }
}