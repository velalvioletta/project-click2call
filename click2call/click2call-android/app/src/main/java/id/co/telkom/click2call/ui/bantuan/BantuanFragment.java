package id.co.telkom.myapplication.ui.bantuan;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import id.co.telkom.myapplication.R;
import id.co.telkom.myapplication.SubActivity;

public class BantuanFragment extends Fragment {

    public BantuanViewModel bantuanViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        bantuanViewModel =
                ViewModelProviders.of(this).get(BantuanViewModel.class);
        View view = inflater.inflate(R.layout.fragment_bantuan, container, false);
        TextView text_debit = (TextView) view.findViewById(R.id.text_debit);
        text_debit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), SubActivity.class);
                in.putExtra("ringing", "ring");
                startActivity(in);
            }
        });
        return view;
    }
}