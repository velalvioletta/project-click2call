package id.co.telkom.myapplication.ui.tagihan;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class TagihanViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public TagihanViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is tools fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}