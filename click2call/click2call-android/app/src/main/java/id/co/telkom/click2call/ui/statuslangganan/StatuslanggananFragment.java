package id.co.telkom.myapplication.ui.statuslangganan;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import id.co.telkom.myapplication.R;

public class StatuslanggananFragment extends Fragment {

    private StatuslanggananViewModel statuslanggananViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        statuslanggananViewModel =
                ViewModelProviders.of(this).get(StatuslanggananViewModel.class);
        View root = inflater.inflate(R.layout.fragment_statuslangganan, container, false);
        final TextView textView = root.findViewById(R.id.text_statuslangganan);
        statuslanggananViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
}