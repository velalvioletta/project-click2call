package id.co.telkom.myapplication.ui.statuslangganan;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class StatuslanggananViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public StatuslanggananViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is slideshow fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}