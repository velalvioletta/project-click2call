<div class="container">

	<?php if ($this->session->flashdata('flash') ) : ?>
	<div class="row mt-3">
		<div class="col-md-6">
			<div class="alert alert-success alert-dismissible fade show" role="alert">
			  Data Pengaduan<strong>berhasil!</strong> <?= 
			  $this->session->flashdata('flash'); ?>.
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			   <span aria-hidden="true">&times;</span>
			  </button>
			</div>
		</div>
	</div>
<?php endif; ?>
	
	<div class="row mt-3">
		<div class="col-md-6">
			<a href="<?= base_url(); ?>report/tambah" class="btn btn-primary">Tambah Data Pengaduan</a>
		</div>
	</div>

	<!-- <div class="row mt-3">
		<div class="col-md-6">
			<form action="" method="post">
				<div class="input-group mb-3">
				  <input type="text" class="form-control" placeholder="Cari data pengaduan.." name="keyword">
				  <div class="input-group-append">
				   	<button class="btn btn-primary" type="submit">Cari</button>
				  </div>
				</div>
			</form>
		</div>
	</div> -->

	<div class="row mt-3">
		<div class="col-md-6">
			<h3>Daftar Pengaduan</h3>
				<ul class="list-group">
					<?php foreach ( $report as $report) :?>
					<li class="list-group-item">
						<?= $report->nama ?>
							<a href="<?= base_url(); ?>report/hapus/<?= $report->id ?>" class="badge badge-danger float-right" onclick="return confirm('yakin');">hapus</a>

							<a href="<?= base_url(); ?>report/ubah/<?= $report->id ?>" class="badge badge-success float-right">ubah</a>

			  				<a href="<?= base_url(); ?>report/detail/<?= $report->id; ?>" class="badge badge-primary float-right">detail</a>
					</li>
   					<?php endforeach?>
			</ul>
		</div>
	</div>
</div>