<div class="container">
		

	<div class="row mt-3">
		<div class="col-md-6">

			<div class="card">
			  <div class="card-header">
			    Form Tambah Data Pengaduan
				  </div>
				 	 <div class="card-body">
						<form action="" method="post">
							<div class="form-group">
								<label for="nomorpelanggan">Nomor Pelanggan</label>
								<input type="text" name="nomorpelanggan" class="form-control" id="nomorpelanggan">
								<small class="form-text text-danger"><?= form_error('nomorpelanggan'); ?> </small>
							</div>
							<div class="form-group">
								<label for="nama">Nama</label>
								<input type="text" name="nama" class="form-control" id="nama">
								<small class="form-text text-danger"><?= form_error('nama'); ?> </small>
							</div>
							<div class="form-group">
								<label for="nomorhp">Nomor HP</label>
								<input type="text" name="nomorhp" class="form-control" id="nomorhp">
								<small class="form-text text-danger"><?= form_error('nomorhp'); ?> </small>
							</div>
							<div class="form-group">
								<label for="pilihan">Keluhan Pelanggan</label>
								<select class="form-control" id="pilihan">
									<option>WIFI.ID</option>
									<option>INDIHOME</option>
									<option>IFLIX</option>
								</select>
								<small class="form-text text-danger"><?= form_error('pilihan'); ?> </small>
							</div>
							<div class="form-group">
								<label for="keluhan">Keterangan Keluhan Pelanggan</label>
								<textarea class="form-control" id="keluhan" rows="4"></textarea>
								<small class="form-text text-danger"><?= form_error('keluhan'); ?> </small>
							</div>
							<div class="form-group">
								<label for="tanggal">Tanggal</label>
								<input type="date" name="tanggal" class="form-control" id="tanggal">
								<small class="form-text text-danger"><?= form_error('tanggal'); ?> </small>
							</div>
							<div class="form-group">
								<label for="jam">Jam</label>
								<input type="time" name="jam" class="form-control" id="jam">
								<small class="form-text text-danger"><?= form_error('jam'); ?> </small>
							</div>
							<button type="submit" name="tambah" class="btn btn-danger">Tambah Data</button>
						</form>
				  </div>
				</div>

		</div>
	</div>
</div>