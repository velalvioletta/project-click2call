<div class="container">
	<div class="row mt-3">
		<div class="col-md-6">
			
			<div class="card">
			  <div class="card-header">
			    Detail Data Report 
			  	</div>
					<?php foreach ( $report as $report) :?>
			  		<div class="card-body">
			    	<h5 class="card-title">Nama : <?= $report->nama ?></h5>
			    	<p class="card-text">Nomor Pelanggan : <?= $report->nomorpelanggan ?></p>
			    	<p class="card-text">Nomor HP : <?= $report->nomorhp ?></p>
			    	<p class="card-text">Jenis Keluhan : <?= $report->pilihan ?></p>
			    	<p class="card-text">Keterangan Keluhan : <?= $report->keluhan ?></p>
			    	<p class="card-text">Tanggal : <?= $report->tanggal ?></p>
			    	<p class="card-text">Jam : <?= $report->jam ?></p>
			    	<a href="<?= base_url(); ?>report" class="btn btn-primary">Kembali Ke Data Report</a>
			  </div>
			  <?php endforeach?>
			</div>	
		</div>
	</div>
</div>