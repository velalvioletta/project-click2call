<div class="container">
		

	<div class="row mt-3">
		<div class="col-md-6">

			<div class="card">
			  <div class="card-header">
			    Form Ubah Data Pengaduan
				  </div>
				 	 <div class="card-body">
						<form action="" method="post">
							<input type="hidden" name="id" value="<?= $report['id']; ?>">
							<div class="form-group">
								<label for="nama">Nama</label>
								<input type="text" name="nama" class="form-control" id="nama" value="<?= $report['nama']; ?>">
								<small class="form-text text-danger"><?= form_error('nama'); ?> </small>
							</div>
							<div class="form-group">
								<label for="nomorhp">Nomor HP</label>
								<input type="text" name="nomorhp" class="form-control" id="nomorhp" value="<?= $report['nomorhp']; ?>">
								<small class="form-text text-danger"><?= form_error('nomorhp'); ?> </small>
							</div>
							<div class="form-group">
								<label for="keluhan">Keluhan Pelanggan</label>
								<input type="text" name="keluhan" class="form-control" id="keluhan" value="<?= $report['keluhan']; ?>">
								<small class="form-text text-danger"><?= form_error('keluhan'); ?> </small>
							</div>
							<div class="form-group">
								<label for="tanggal">Tanggal</label>
								<input type="date" name="tanggal" class="form-control" id="tanggal" value="<?= $report['tanggal']; ?>">
								<small class="form-text text-danger"><?= form_error('tanggal'); ?> </small>
							</div>
							<div class="form-group">
								<label for="jam">Jam</label>
								<input type="time" name="jam" class="form-control" id="jam" value="<?= $report['jam']; ?>">
								<small class="form-text text-danger"><?= form_error('jam'); ?> </small>
							</div>
							<button type="submit" name="ubah" class="btn btn-danger">Ubah Data</button>
						</form>
				  </div>
				</div>

		</div>
	</div>
</div>