<div class="container">

  <?php if ($this->session->flashdata('flash') ) : ?>
  <div class="row mt-3">
    <div class="col-md-6">
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        Gambar<strong>berhasil!</strong> <?= 
        $this->session->flashdata('flash'); ?>.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
         <span aria-hidden="true">&times;</span>
        </button>
      </div>
    </div>
  </div>
<?php endif; ?>

<div class="row mt-3">
    <div class="col-md-6">
      <a href="<?= base_url(); ?>gambar/tambah" class="btn btn-primary">Upload</a>
    </div>
</div>


<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Deskripsi</th>
      <th scope="col">Nama File</th>
      <th scope="col">Ukuran File</th>
      <th scope="col">Tipe File</th>
      <th scope="col">Status</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($gambar as $gambar) :?>
    <tr>
    <td>
      <img src="<?= base_url("images/".$gambar->nama_file) ?>"
          width='100' height='100' >
    </td>
    <td><?= $gambar->deskripsi ?></td>
    <td><?= $gambar->nama_file ?></td>
    <td><?= $gambar->ukuran_file ?></td>
    <td><?= $gambar->tipe_file ?></td>
    <td>
      <?php
        $status = $gambar->status;
        echo $status;
        if ($status == 0) 
        {
        ?> 
            <a href="<?php echo site_url('gambar/update_status'.'?sid='.$gambar->id.'&sval='.$status); ?>" class="btn btn-success">Post</a>
        <?php
        } else {
          ?>
            <a href="<?php echo site_url('gambar/update_status'.'?sid='.$gambar->id.'&sval='.$status); ?>" class="btn btn-warning">Unpost</a>
        <?php
        }
        ?>
    </td>

    </tr>
  <?php endforeach?>
  </tbody>
</table>