<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gambar extends CI_Controller {

	var $API ="";
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('Gambar_Model');
		$this->API="http://localhost/rest_ci/index.php";
		$this->load->model('Gambar_Model');
		$this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->helper('form');
        $this->load->helper('url');
	}
	
	public function index(){
		$data['judul'] = 'Data Gambar';
		$data['report'] = json_decode($this->curl->simple_get($this->API.'/gambar'));
		$data['gambar'] = $this->Gambar_Model->view();

		$this->load->view('templates/header', $data);
		$this->load->view('gambar/index', $data);
		$this->load->view('templates/footer');
	}
	
	public function tambah(){

		$data['judul'] = 'Form Upload Gambar';

		$data = array(
				'id'			=> $this->input->post('id'),
				'deskripsi'		=> $this->input->post('deskripsi'),
				'nama_file' 	=> $this->input->post('nama_file'),
				'ukuran_file'	=> $this->input->post('ukuran_file'),
				'tipe_file'		=> $this->input->post('tipe_file'));
		
		if($this->input->post('submit')){ // Jika user menekan tombol Submit (Simpan) pada form
			// lakukan upload file dengan memanggil function upload yang ada di GambarModel.php
			$upload = $this->Gambar_Model->upload();
			
			if($upload['result'] == "success"){ // Jika proses upload sukses
				 // Panggil function save yang ada di GambarModel.php untuk menyimpan data ke database
				$this->curl->simple_post($this->API.'/gambar', $data, array(CURLOPT_BUFFERSIZE => 10)); 
				$this->Gambar_Model->save($upload);
				
				redirect('gambar'); // Redirect kembali ke halaman awal / halaman view data
			}else{ // Jika proses upload gagal
				$data['message'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
			}
		}
		
		$this->load->view('templates/header', $data);
		$this->load->view('gambar/tambah', $data);
		$this->load->view('templates/footer');
	}

	public function update_status()
	{
		if (isset($_REQUEST['sval'])) 
		{
			$this->load->model('Gambar_Model','gambar');

			$up_status = $this->gambar->update_status();

			if ($up_status>0) 
			{
				//$data['report'] = json_decode($this->curl->simple_put($this->API.'/report', $data, array(CURLOPT_BUFFERSIZE => 10)));
				$this->session->set_flashdata('msg', "Status Berhasil Diupdate");
				$this->session->set_flashdata('msg_class','alert-success');
			}
			else
			{
				$this->session->set_flashdata('msg', "Status Tidak Berhasil Diupdate");
				$this->session->set_flashdata('msg_class','alert-danger');
			}
			return redirect('gambar/index');
		}
	}
}