<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

	var $API ="";

	public function __construct()
	{
		parent::__construct();
		$this->API="http://localhost/rest_ci/index.php";
		$this->load->model('Report_model');
		$this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->helper('form');
        $this->load->helper('url');
	}

	public function index()
	{
		$data['judul'] = 'Daftar Pengaduan';
		$data['report'] = json_decode($this->curl->simple_get($this->API.'/report'));
		//print_r($data['report']); die;

		//if ($this->input->post('keyword')) {
		//$data['report'] = $this->Report_model->cariDataReport();	}
		
		$this->load->view('templates/header', $data);
		$this->load->view('report/index', $data);
		$this->load->view('templates/footer');
	}

	public function tambah()
	{

		$data['judul'] = 'Form Tambah Data Report';

		$data = array(
                'id'       			=>  $this->input->post('id'),
                'nomorpelanggan'    =>  $this->input->post('nomorpelanggan'),
                'nama'      		=>  $this->input->post('nama'),
                'nomorhp'      		=>  $this->input->post('nomorhp'),
                'pilihan'   		=>  $this->input->post('pilihan'),
                'keluhan'      		=>  $this->input->post('keluhan'),
                'tanggal'      		=>  $this->input->post('tanggal'),
                'jam'				=>  $this->input->post('jam'));

		$this->form_validation->set_rules('nomorpelanggan', 'NomorPelanggan', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('nomorhp', 'NomorHP', 'required|numeric');
		$this->form_validation->set_rules('pilihan', 'Pilihan', 'required');
		$this->form_validation->set_rules('keluhan', 'Keluhan', 'required');
		$this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
		$this->form_validation->set_rules('jam', 'Jam', 'required');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('templates/header', $data);
			$this->load->view('report/tambah', $data);
			$this->load->view('templates/footer');
		} else {
			$this->curl->simple_post($this->API.'/report', $data, array(CURLOPT_BUFFERSIZE => 10)); 
			$this->session->set_flashdata('flash', 'Berhasil ditambahkan');
			redirect('report');
		}
		
	}

	public function ubah($id)
	{
		$data['judul'] = 'Form Tambah Data Report';

		$data = array(
                'id'  	   			=>  $this->input->post('id'),
                'nomorpelanggan'  	=>  $this->input->post('nomorpelanggan'),
                'nama'      		=>  $this->input->post('nama'),
                'nomorhp'      		=>  $this->input->post('nomorhp'),
                'pilihan'  	   		=>  $this->input->post('pilihan'),
                'keluhan'      		=>  $this->input->post('keluhan'),
                'tanggal'      		=>  $this->input->post('tanggal'),
                'jam'				=>  $this->input->post('jam'));

		$this->form_validation->set_rules('nomorpelanggan', 'NomorPelanggan', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('nomorhp', 'NomorHP', 'required|numeric');
		$this->form_validation->set_rules('pilihan', 'Pilihan', 'required');
		$this->form_validation->set_rules('keluhan', 'Keluhan', 'required');
		$this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
		$this->form_validation->set_rules('jam', 'Jam', 'required');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('templates/header', $data);
			$this->load->view('report/ubah', $data);
			$this->load->view('templates/footer');
		} else {

			$data['report'] = json_decode($this->curl->simple_put($this->API.'/report', $data, array(CURLOPT_BUFFERSIZE => 10)));
			$this->session->set_flashdata('flash', 'Berhasil diubah');
			redirect('report');
		}
	}	

	public function hapus($id)
	{
		$this->curl->simple_delete($this->API.'/report', array('id'=>$id), array(CURLOPT_BUFFERSIZE => 10));
		$this->session->set_flashdata('flash', 'Dihapus');
		redirect('report');
	}

	public function detail($id)
	{
		$data['judul'] = 'Detail Data Report';
		
		$data['report'] = json_decode($this->curl->simple_get($this->API.'/report', array('id'=>$id), array(CURLOPT_BUFFERSIZE => 10)));

		$this->load->view('templates/header', $data);
		$this->load->view('report/detail', $data);
		$this->load->view('templates/footer');
		}
	}