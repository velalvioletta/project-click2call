<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller 
{ 

    var $API ="";

    public function __construct()
    {
        parent::__construct();
        $this->API="http://localhost/rest_ci/index.php";
        $this->load->model('UserModel');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->helper('form');
        $this->load->helper('url');
    }

    public function index()
    {
        $data['admin'] = json_decode($this->curl->simple_get($this->API.'/auth'));

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

         if($this->session->userdata('authenticated')) // Jika user sudah login (Session authenticated ditemukan)
          redirect('home'); // Redirect ke page welcome

      $this->load->view('templates/auth_header');
      $this->load->view('login');
      $this->load->view('templates/auth_footer');
  }

   public function login(){
    
    
    $email = $this->input->post('email'); // Ambil isi dari inputan username pada form login
    $password = ($this->input->post('password')); // Ambil isi dari inputan password pada form login dan encrypt dengan md5

    $user = $this->db->get_where('admin', ['email' => $email])->row(); // Panggil fungsi get yang ada di UserModel.php

    if(empty($user)){ // Jika hasilnya kosong / user tidak ditemukan
      $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Akun tidak terdaftar! Silakan hubungi teknisi untuk mendaftarkan akun<div>'); // Buat session flashdata
      redirect('auth'); // Redirect ke halaman login
    }else{
      if($password == $user->password){ // Jika password yang diinput sama dengan password yang didatabase
        $session = array(
          'authenticated'=>true, // Buat session authenticated dengan value true
          'email'=>$user->email,  // Buat session username
          'nama'=>$user->nama // Buat session authenticated
        );

        $this->session->set_userdata($session); // Buat session sesuai $session
        redirect('home'); // Redirect ke halaman welcome
      }else{
        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password Salah<div>'); // Buat session flashdata
        redirect('auth'); // Redirect ke halaman login
      }
    }
  }

  public function logout(){
    $this->session->sess_destroy(); // Hapus semua session
    redirect('auth'); // Redirect ke halaman login
  }
}