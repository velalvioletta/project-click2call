<?php

class Report_model extends CI_Model {

	public function getReportById($id)
	{
		return $this->db->get_where('report', ['id' => $id])->row_array();
	}

	public function ubahDataReport()
	{
		$data = [
			"nama" => $this->input->post('nama', true),
			"nomorhp" => $this->input->post('nomorhp', true),
			"keluhan" => $this->input->post('keluhan', true),
			"tanggal" => $this->input->post('tanggal', true),
			"jam" => $this->input->post('jam', true),
		];

		$this->db->where('id', $this->input->post('id'));
		$this->db->update('report', $data);
	}

	public function cariDataReport()
	{
		$keyword = $this->input->post('keyword', true);
		$this->db->like('nama', $keyword);
		$this->db->or_like('nomorhp', $keyword);
		return $this->db->get('report')->row_array();
	}
}