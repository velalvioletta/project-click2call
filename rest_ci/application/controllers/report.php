<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class report extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    //Menampilkan data kontak
    function index_get() {
        $id = $this->get('id');
        if ($id == '') {
            $click2call = $this->db->get('report')->result_array();
        } else {
            $this->db->where('id', $id);
            $click2call = $this->db->get('report')->result_array();
        }
        $this->response($click2call, 200);
    }

     //Mengirim atau menambah data baru
    function index_post() {
        $data = array(
                    'id'                => $this->post('id'),
                    'nomorpelanggan'    => $this->post('nomorpelanggan'),
                    'nama'              => $this->post('nama'),
                    'nomorhp'           => $this->post('nomorhp'),
                    'pilihan'           => $this->post('pilihan'),
                    'keluhan'           => $this->post('keluhan'),
                    'tanggal'           => $this->post('tanggal'),
                    'jam'               => $this->post('nomorhp'));
        $insert = $this->db->insert('report', $data);
        if ($insert) {
            $this->response($data, 200) ;
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    //Memperbarui data  yang telah ada
    function index_put() {
        $id = $this->put('id');
        $data = array(
                    'id'                   => $this->put('id'),
                    'nomorpelanggan'       => $this->put('nomorpelanggan'),
                    'nama'                 => $this->put('nama'),
                    'nomorhp'              => $this->put('nomorhp'),
                    'pilihan'              => $this->put('pilihan'),
                    'keluhan'              => $this->put('keluhan'),
                    'tanggal'              => $this->put('tanggal'),
                    'jam'                  => $this->put('jam'));
        $this->db->where('id', $id);
        $update = $this->db->update('report', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    //Menghapus salah satu data
    function index_delete() {
        $id = $this->delete('id');
        $this->db->where('id', $id);
        $delete = $this->db->delete('report');
        if ($delete) {
            $this->response(array('status' => 'success'), 201);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}
?>