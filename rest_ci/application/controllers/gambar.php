<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class gambar extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    //Menampilkan data kontak
    function index_get() {
        $id = $this->get('id');
        if ($id == '') {
            $this->db->where('status', '1');
            $click2call = $this->db->get('gambar')->result_array();
        } else {
            $this->db->where('id', $id);
            $click2call = $this->db->get('gambar')->result_array();
        }
        $this->response($click2call, 200);
    }

     //Mengirim atau menambah data baru
    function index_post() {
        $data = array(
                    'id'           => $this->post('id'),
                    'deskripsi'    => $this->post('deskripsi'),
                    'nama_file'    => $this->post('nama_file'),
                    'ukuran_file'  => $this->post('ukuran_file'),
                    'tipe_file'    => $this->post('tipe_file'));
        $insert = $this->db->insert('gambar', $data);
        if ($insert) {
            $this->response($data, 200) ;
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    //Memperbarui data  yang telah ada
    function index_put_post() {
        $id = $this->put('id');
        $data = array(
                    'status'       => '1');

        $this->db->where('id', $id);
        $update = $this->db->update('gambar', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));

        }
    }

    function index_put_unpost() {
        $id = $this->put('id');
        $data = array(
                    'status'       => '0');
                    
        $this->db->where('id', $id);
        $update = $this->db->update('gambar', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));

        }
    }

    //Menghapus salah satu data
    function index_delete() {
        $id = $this->delete('id');
        $this->db->where('id', $id);
        $delete = $this->db->delete('gambar');
        if ($delete) {
            $this->response(array('status' => 'success'), 201);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    function update_status()
    {
        if (isset($_REQUEST['sval'])) 
        {
            $this->load->model('Gambar_Model','gambar');

            $up_status = $this->gambar->update_status();

            if ($up_status>0) 
            {
                $this->session->set_flashdata('msg', "Status Berhasil Diupdate");
                $this->session->set_flashdata('msg_class','alert-success');
            }
            else
            {
                $this->session->set_flashdata('msg', "Status Tidak Berhasil Diupdate");
                $this->session->set_flashdata('msg_class','alert-danger');
            }
            return redirect('gambar/index');
        }
    }
}
?>