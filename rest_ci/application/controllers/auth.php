<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class auth extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    //Menampilkan data kontak
    function index_get() {
        $id = $this->get('id');
        if ($id == '') {
            $click2call = $this->db->get('admin')->result_array();
        } else {
            $this->db->where('id', $id);
            $click2call = $this->db->get('admin')->result_array();
        }
        $this->response($click2call, 200);
    }

     //Mengirim atau menambah data baru
    function index_post() {
        $data = array(
                    'id'                => $this->post('id'),
                    'email'             => $this->post('email'),
                    'password'          => $this->post('password'));
        $id = $this->get('id');
        if ($id == '') {
            $click2call = $this->db->get('admin')->result_array();
        } else {
            $this->db->where('id', $id);
            $click2call = $this->db->get('admin')->result_array();
        }
        $this->response($click2call, 200);
        }
    }

    //Memperbarui data  yang telah ada
    function index_put() {
        $id = $this->put('id');
        $data = array(
                    'id'                   => $this->put('id'),
                    'email'                => $this->put('email'),
                    'password'             => $this->put('password'));
        $this->db->where('id', $id);
        $update = $this->db->update('admin', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    //Menghapus salah satu data
    function index_delete() {
        $id = $this->delete('id');
        $this->db->where('id', $id);
        $delete = $this->db->delete('admin');
        if ($delete) {
            $this->response(array('status' => 'success'), 201);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}
?>